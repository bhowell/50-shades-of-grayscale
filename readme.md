# 50 Shades of Grayscale
## *tools for design experiments with receipt printers*
---
*Created June, 2019*

*For the workshop __When in Doubt, Print it Out__ held at [LGM 2019](https://libregraphicsmeeting.org/2019/)*

![Sample Output](./output.jpg)

This repo contains a basic setup and several examples, in python, for using industry standard ESC-POS printers to print a variety of text and images. These printers can be bought used (the Epson TM-88 series is highly recommended) or new (knock-offs can be found on ebay for 20-30€) and enable all kinds of light fun and dark dirty (dithered) pleasure.

This work is part of a [larger effort](http://screenl.es/) effort to encourage more diversity in interaction and less screen-centered everyday life. 

### Set-Up

This stuff is tested on Linux but you might be able to get some of it working on a Mac. Windows might work but it will probably need some hacking to get the hardware talking.

1. Plug in your printer. It needs to be ESC-POS compatible.
2. You need python3. (It's the 21st Century!) TODO: distro requirements here.
3. Create a new virtual environment and install the requirements:

```bash
sudo apt install git build-essential python3-dev libjpeg-dev libcairo2-dev
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

Once all the libs are installed you are ready to run the scripts. Next time you can just run `source env/bin/activate` before you start hacking.

### Web App

There is a super-simple (totally un-styled) web-app which will provide an interface that will let you print images and pre-formatted text. Make yourself a 'zine, wallpaper, narrow christmas cards, fake your taxes, ASCII elder-scrolls, etc. 

Just run:
`python printer.py` and open `http://localhost:8080`.

If you have friends (on the same LAN) and you want to share in the fun, you can find your local IP address and have them connect to your machine. Just substitute `localhost` in the URL with your IP. 

The app is poorly tested and will probably crash a lot. It gets especially temperamental with PNGs that are not 8-bit or have an alpha channel. Don't panic. Just restart the server and save the image in another format.

Also, this app is probably very insecure so don't run it anywhere if you can't trust people on your local LAN.

### Roll Your Own Scripts

If you want to get started writing your own printing routines, take a look at the scripts in the `examples/` folder. If you can write a little code, it's not too hard to get started turning out images and text that come from other data sources or applications.

The script `repl1.py` shows how to use a simple REPL (Read-Evaluate-Print Loop) to take input from the keyboard (or even cooler a *Bar-code Scanner*) and to print output from terminal commands. `repl2.py` opens and prints the contents of files. `printimage.py` is a slightly fancier script that will print images from the command line.

These scripts were all done using the very convenient [python-escpos](https://python-escpos.readthedocs.io) library. If you need to get fancier, take a look at their docs.

Enjoy!

![Sample Twitter Output](./tweet.jpg)
