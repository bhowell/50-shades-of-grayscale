# quick and dirty tiny-printer-chat
# this is an extremely primitive tiny-printer-based 2-way messaging application
# it has only be used on a LAN - it will likely not work through firewalls
# But feel free to hack it!
import configparser
import socket
import threading

from escpos import printer

MY_NAME = "Borf"
SERVER, PORT = "localhost", 2024
PRNT = None
PRNT_LOCK = threading.Lock()


def main_input():
    while True:
        msg = input(">")
        msg = MY_NAME + ": " + msg
        print(msg)
        with PRNT_LOCK:
            PRNT.text(msg)
            PRNT.textln("\n")
            pass

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((SERVER, PORT))
            sock.sendall(bytes(msg + "\n", "utf-8"))


def talk_server():
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind(('', PORT))
            sock.listen(1)
            conn, addr = sock.accept()
            msg = ""
            with conn:
                print("<msg from", addr, ">")
                data = conn.recv(1024)
                while data:
                    msg += data.decode("utf-8")
                    data = conn.recv(1024)
                print(msg)


if __name__ == "__main__":
    cfg = configparser.ConfigParser()
    cfg.read("printer.cfg")

    prn_type = cfg["printer"]["type"].lower()
    if prn_type == "file":
        PRNT = printer.File(cfg["printer"]["device"])
    elif prn_type == "usb":
        vendorID, productID = cfg["printer"]["device"].split(":")
        PRNT = printer.Usb(int(vendor, 16), int(product, 16))

    # run SERVER
    serv = threading.Thread(target=talk_server)
    serv.start()

    # run main_input loop
    main_input()
