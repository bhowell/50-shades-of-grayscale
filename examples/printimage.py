#!/usr/bin/env python
"""
This is a simple script that prints an image on an escpos capable printer
which is configured in printer.cfg. 
"""
import configparser
import os
import sys

from escpos import printer
import PIL

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit('Usage: %s [-cut] <image file>' % sys.argv[0])

    im_file = sys.argv[-1]
    if not os.path.exists(im_file):
        print("Error! Image file not found.", file=sys.stderr)
        sys.exit('Usage: %s [-cut] <image file>' % sys.argv[0])


    cfg = configparser.ConfigParser()
    cfg.read("printer.cfg")

    prn_type = cfg["printer"]["type"].lower()
    prn_device = cfg["printer"]["device"]
    prn_width = int(cfg["printer"]["width"])
    if prn_type == "file":
        prn = printer.File(prn_device)
    elif prn_type == "usb":
        vendor, product = prn_device.split(":")
        prn = printer.Usb(hex(int(vendor, base=16)), 
                          hex(int(product, base=16)))
    
    im = PIL.Image.open(im_file)
    im = PIL.ImageOps.equalize(im)
    im.thumbnail((prn_width, 900), PIL.Image.ANTIALIAS)

    prn.image(im, impl="bitImageColumn")

    if sys.argv[-2] == "-cut":
        prn.cut()

    prn.close()
