# this is a simple screenless REPL / CLI
import configparser
import os
import subprocess

from escpos import printer
import PIL

cfg = configparser.ConfigParser()
cfg.read("printer.cfg")
prn_type = cfg["printer"]["type"].lower()
prn_device = cfg["printer"]["device"]
prn_width = int(cfg["printer"]["width"])

while True:
    msg = input("> ")
    print("got message: " + msg)
    if prn_type == "file":
        prn = printer.File(prn_device)
    elif prn_type == "usb":
        vendor, product = prn_device.split(":")
        prn = printer.Usb(int(vendor, 16),
                          int(product, 16))

    if msg.startswith("PR."):
        _, item = msg.split(".")
        # Here we just open a simple file but
        # you could look up an item in a DB
        # or key-val store, etc.
        # note: this is extremely insecure
        with open(item + ".txt") as item_text:
            prn.text(item_text.read())

    # ADD YOUR AMAZING COMMANDS HERE

    prn.close()
