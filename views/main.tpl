<!DOCTYPE html>
<html>
<head>
  <title>When in doubt, print it out!</title>
  <meta charset="UTF-8"> 
</head>
<body>
  <h1>50 Shades of Grayscale</h1>
  <h2>{{msg}}</h2>
  <form action="/print_img" method="post" enctype="multipart/form-data">
    Print image: <br/>
    <input type="file" name="pr_img" /> <br />
    Equalize Levels: <input type="checkbox" name="equalize" /> <br />
    <input type="submit" value="upload &amp; print!" />
  </form>
  <hr />
  <form action="/print_text" method="post">
    Bold: <input type="checkbox" name="bold" /> <br />
    Underline: <input type="checkbox" name="underline" /> <br />
    Invert: <input type="checkbox" name="invert" /> <br />
    Print text: <textarea name="pr_text" rows="5" cols="48"></textarea> <br />
    <input type="submit" value="print!" />
  </form>
  <hr />
  <form action="/print_bc" method="post">
    Barcode: <textarea name="bc_text" rows="5" cols="48"></textarea>
    <input type="submit" value="print BC!" />
  </form>
  <hr />
  <form action="/print_qr" method="post">
    QR Code: <textarea name="qr_text" rows="5" cols="48"></textarea>
    <input type="submit" value="print QR!" />
  </form>
  <hr />
  <form action="/cut" method="get">
    <input type="submit" value="CUT THE PAPER!" />
  </form>
</body>
</html>
