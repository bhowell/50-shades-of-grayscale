"""
50 Shades of Grayscale
a simple web app for printing on ESC-POS receipt printers

Copyright 2019 - Brendan Howell
GPL-3
"""
import configparser
import os
import tempfile

from bottle import request, route, run, static_file, view, redirect, response
from escpos import printer
import PIL

prn = None
nogfx = False


@route("/")
@view("main")
def index():
    print(request.cookies.keys())
    msg = request.cookies.msg or ""
    if msg:
        response.set_cookie("msg", "")
    return dict(msg=msg, nogfx=nogfx)


@route("/print_img", method="POST")
def print_img():
    upload = request.files.get("pr_img")
    img, ext = os.path.splitext(upload.filename)
    if ext.lower() not in (".png", ".jpg", ".jpeg"):
        msg = "Error: image type not allowed. Use PNG or JPG."
    else:
        msg = "Printed " + upload.filename + " OK"

    fd, save_path = tempfile.mkstemp(ext)
    upload.save(save_path, overwrite=True)

    # now do the printing
    im = PIL.Image.open(save_path)
    equalize = request.forms.get("equalize")
    if equalize:
        im = PIL.ImageOps.equalize(im)
    im.thumbnail((prn_width, 900), PIL.Image.Resampling.LANCZOS)
    try:
        # prn.image(im, impl="bitImageColumn")
        prn.image(im)
    except Exception as err:
        msg = str(err)

    response.set_cookie("msg", msg)

    redirect("/")


@route("/print_bc", method="POST")
def print_bc():
    bc_text = request.forms.bc_text
    if len(bc_text) < 12:
        try:
            prn.barcode(bc_text, "CODE128", function_type="B",
                        force_software="bitImageRaster")
            msg = "Barcode printed."
        except escpos.exceptions.BarcodeCodeError:
            msg = "Barcode contains invalid characters."
    else:
        msg = "ERROR Barcode too long!"

    response.set_cookie("msg", msg)
    redirect("/")


@route("/print_qr", method="POST")
def print_qr():
    qr_text = request.forms.qr_text
    try:
        prn.qr(qr_text)
        msg = "QR Code printed"
    except Exception as err:
        msg = str(err)

    response.set_cookie("msg", msg)
    redirect("/")


@route("/print_text", method="POST")
def print_text():
    def setting_to_bool(val):
        if val == "on":
            return True
        else:
            return False

    text = request.forms.pr_text
    bold = setting_to_bool(request.forms.get("bold"))
    underline = setting_to_bool(request.forms.get("underline"))
    invert = setting_to_bool(request.forms.get("invert"))
    print("text: ", text, "bold:", bold, "underline", underline, "invert",
          invert)

    # now print it out
    try:
        prn.set(bold=bold, underline=underline, invert=invert)
        prn.text(text)
        response.set_cookie("msg", "Your text has been printed.")
    except UnicodeEncodeError:
        response.set_cookie("msg",
                            "ERROR: your text has some funky characters.")

    prn.set()  # reset to default text formatting
    redirect("/")


@route("/cut")
def cut():
    # cut the paper
    prn.cut()
    response.set_cookie("msg", "Paper has been cut!")
    redirect("/")


@route('/static/<filename:path>')
def send_static(filename):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    static_path = os.path.join(dir_path, "static")
    return static_file(filename, root=static_path)


if __name__ == "__main__":

    cfg = configparser.ConfigParser()
    cfg.read("printer.cfg")

    if "nogfx" in cfg["printer"]:
        nogfx = cfg["printer"]["nogfx"]

    prn_type = cfg["printer"]["type"].lower()
    prn_device = cfg["printer"]["device"]
    prn_width = int(cfg["printer"]["width"])
    if prn_type == "file":
        prn = printer.File(prn_device)
    elif prn_type == "ip":
        prn = printer.Network(cfg["printer"]["device"])
    elif prn_type == "usb":
        vendor, product = prn_device.split(":")
        prn = printer.Usb(int(vendor, 16),
                          int(product, 16))

    # hack - change this to "EURO" normally and remove the codepage = blah
    # prn.charcode("LATIN2")
    # prn.codepage = "cp858"
    run(host='0.0.0.0', port=8080, debug=True)
